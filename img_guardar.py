import io
from PIL import Image


class MiImagen:
    def desde_marcador(self, imgx, nombre='imagen.jpg'):
        with open(nombre, 'wb') as file:
            file.write(imgx)

class MiImagenConPillow:
    def guarda_imagen(self, imgx, w=100, nombre="imagen.jpg"):
        img = self.quita_bordes_negros(imgx)
        image = self.tam(img, w)
        image.save(nombre)

    def quita_bordes_negros(self, img, md=16):
        imagen = io.BytesIO(img)
        img_pil = Image.open(imagen)
        w, h = img_pil.size
        return img_pil.crop((0,md,w,h-md))
    
    def tam(self, img, w=120):
        return img.resize((w, int(w/1.7778)))


if __name__=="__main__":
    from mi_pbf import MiPbf
    from pprint import pprint

    mp = MiPbf(r'./pbf/boo_zr.pbf')
    m1 = mp.marcadores(con_imagen=True)[0]
    img1 = m1.get('imagen hex')

    # GUARDA IMAGEN BASICO
    # mi = MiImagen()
    # mi.desde_marcador(img1)

    # GUARDA IMAGEN Y REDIMENSIONA Y QUITA BORDES CON PILLOW
    # cpil = MiImagenConPillow()
    # cpil.guarda_imagen(img1)