# Tiempo de PBF
## MODULOS
### mi_pbf
modulo para obtener data de un archivo **.pbf** que es una extension del reporductor potplayer.

Obtiene una lista de diccionarios, cada diccionario lleva datos de un marcador (h,m,s,ms,timestamp, tiempo acortado, imagen previa)
> ![INFO] imagen
> la Imagen esta en formato hexadecimal al momento de obtener informacion de un marcador, el obtener imagen es opcional (por que es un texto bastante largo y la imagen es muy pequeña)

para guardar la imagen se puede usar el modulo **img_guardar**
### img_guardar
este modulo solo es para guardar la imagen hexadecimal que se obtiene con **mi_pbf** para guardar como archivo

existe otra clase que puede guardar el archivo usando **PIL** este quita los borde negros (que tiene por defecto), redimensiona (aunque la imagen generada por defecto es pequeña) por lo que la imagen es de mala calidad

> ![INFO] imagen es pequeña
> para que ocupe menos texto (ya que se guarda como texto)

