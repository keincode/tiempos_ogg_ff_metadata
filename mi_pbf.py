from pathlib import Path


class MiPbf:
    def __init__(self, archivo_video=None):
        self.VIDEO = None
        self.PBF = None
        if archivo_video:
            self.video = archivo_video

    @property
    def video(self):
        return self.VIDEO
    
    @video.setter
    def video(self, archivo):
        file = Path(archivo)
        self.VIDEO = file.as_posix()
        self.PBF = file.with_suffix('.pbf').as_posix()

    def _lee(self):
        with open(self.PBF, 'r', encoding='utf-16') as txt:
            return [l.strip('\n') for l in txt.readlines()]
        
    def mseg_a_hmsz(self, msegundos):
        """retorna tupla(enteros) =  h, m, s, z"""
        h, r = divmod(int(msegundos), 3.6e6)
        m, r = divmod(r, 6e4)
        s, _ = divmod(r, 1e3)
        return int(h), int(m), int(s), int(_)
    
    def mseg_a_timestamp(self, msegundos):
        h, m, s, ms = self.mseg_a_hmsz(msegundos)
        tiempo = f"{h:02d}:{m:02d}:{s:02d}.{ms:03d}"
        t = tiempo[3::] if tiempo.startswith('00:') else tiempo
        return {
            'horas':h, 'minutos':m,
            'segundos':s, 'milisegundos':ms,
            'tiempo':tiempo, 't':t
        }
    
    def marcadores(self, con_imagen=False):
        "retorna una lista de diccionarios, un diccionario por cada marcador"
        lista = []
        marcador = iter(self._lee())
        if '[Bookmark]' in next(marcador):
            linea = next(marcador)
            if '*' in linea:
                _, data = linea.split('=')
                mseg, titulo, image = data.split('*')
                d = {'mseg':int(mseg), 'titulo':titulo, **self.mseg_a_timestamp(mseg)}
                if con_imagen:
                    d['imagen hex']= bytes.fromhex(image.rsplit('0'*43)[-1])
                lista.append(d)
        return lista


if __name__=="__main__":
    from pprint import pprint
    md = MiPbf()
    mi_pbf = r'./pbf/boo_zr.pbf'
    md.video = mi_pbf
    a = md.marcadores()
    pprint(a[0])